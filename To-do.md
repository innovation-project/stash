# List of Things To-Do

## Organisation
- [ ] Compile all of our sensor's documentation together.

## Hardware
- [ ] Connect sensors.
- [ ] Figure out what we're doing with the SPI bridges.

## Software
- [ ] Weather Forecasting API.
- [ ] Get data input from sensors into a Python script or something.
- [ ] Make visualisations, likely in Grafana.
- [ ] Make login system, security.
- [ ] AI integration

## Weekly To-do
- [ ]  Week 14 [2019-04-01 to 2019-04-07]
    - [ ] Collecting data from at least two sensors. (Hardware/Electronics)
    - [ ] *Critical Review Meeting #2*
    - [x] Finish Static Website Frontend
- [ ]  Week 15 [2019-04-08 to 2019-04-14]

- [ ]  Week 16 [2019-04-15 to 2019-04-21]


- [ ]  Week 17 [2019-04-22 to 2019-04-28]


- [ ]  Week 18 [2019-04-29 to 2019-05-05]


- [ ]  Week 19 [2019-05-06 to 2019-04-12]


- [ ]  Week 20 [2019-05-13 to 2019-05-19]


- [ ]  Week 21 [2019-05-20 to 2019-05-26]


- [ ]  Week 22 [2019-05-26 to 2019-04-30]
    - [ ]  *Final Project Presentation Here.*