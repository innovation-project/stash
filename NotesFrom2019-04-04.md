# Notes from 2019-04-04 lecture.

Okay, right, good morning to you all!
Good morning to you all!
Good morning to you all.
That's it give me some energy. Enthusiasm!
That's what I like to see on this glorious day.

We will be doing DHCP and there's a reminder that we have an assessment this week.
We will be required to do a Netacad Cisco online test thing. It's for a number of reasons:
* Gives us an insight into the certification process.
* It's a part of learning in the work place
* it supports employability, our university focuses on employability a lot.
* It gives you an insight into certification
* And the skills in employability and what the standard is expected of you.

Certifications are usually expected to be 85%, you need to be an expert.
It's increasingly more difficult for employers to differentiate between someone with a first-class degree, someone with a first-class degree, and someone with a first-class degree.

It will be taken under standard University assessment regulations.
*No electronics.*

You're not allowed to talk, you're not allowed to go on to search engines like Google or Bing or whatever to search for answers. You're not allowed to look at the curriculum. You're not allowed to cheat.
* It will be 1h 45mins. We need to stay for at least 1 hour.

If you break any of these conditions, you'll be subject for cheating/plagiarism etc.

We will see our results straight away. As well as get feedback.

You have nothing to worry about as long as you're properly prepared. You have to get organised as if you're executing it with military precision.

Fault tolerance is implemented with both extra hardware and extra cables.

Trunking can provide multiple channels of communication between gateways/routers/switches. The design at Joseph Priestley are a rudimentary example of redundancy.

Amazon talk about 99.9% of uptime.

You have stateful packet inspection, intrusion detection, intrusion prevention, anti-phishing software.

Apple devices tend not to have viruses because are designed to be multi-casting systems. They were built from the ground up for networking.

More subnets means a smaller broadcast domain.
You still have the ability to communicate between subnets because you have a router there, but you have performance gains because you have two subnets and a smaller broadcast domain. You also get the security benefits.

*University have experienced nearly exponential growth in terms of students.*

35,000 people come to this University this year. 35,000 people on a single subnet is just not going to work, is it? You must think about scalability and you must think about subnetting.

`show ip interface brief`

We looked at `show arp`, address resolution protocol. Which part of the encapsulation process are we using the MAC address to complete? Frames? Frames, yes!

Every machine on a large network using ARP, it can cause problems as it's a broadcast-style process. If ARP ain't working, you can't build frames, which means that you can't communicate.

<br><img src="https://i.imgur.com/dkHQSpp.png">

The router will show you connected routes provided that you put IP addresses on the routers.
Those IP addresses will be part of a network which is designated by your packet-tracer activity which you get as a result of your profile. You'll get a LAN address and a WAN address for each site and they'll automatically be connected to a networking table and revealed by default by a router when you use `show ip routes`.

This means that you can connect to locally connected networks. There's a little key showing how the router has discovered those networks. The router doesn't know about remote routes but it doesn't know about them. Like remote networks, you need to tell it about those. You need to configure routes so that the router knows about remote networks.

`show ip interface brief`

**This week: IP,** configuring IP, as you can imagine, it's okay if you can configure an IP address for a small network manually however as your network begins to grow, imagine everyone here each week, each student needs to configure their own IP address, gateway address, subnet mask, DNS server etc.

As far as the IETF *(Internet Engineering Task Force)* is concerned, they use DHCP. Before that, they used BOOTP.

## DHCP Demonstration Video

<iframe width="560" height="315" src="https://www.youtube.com/embed/NF-siZvV7oM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/cjh0-T1Hux8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>