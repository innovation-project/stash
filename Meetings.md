# Internal Project Meetings
**This document will operate like a stack.** New meetings are added to the top. Old meetings get pushed down. This document will be in chronological order.
The next project meeting that hasn't happened yet will be in *italics.*
## Current Project Meeting

* **Location:** C118
* **Time:** 11:00 - 13:00 (Booked by Snuggle [C118 | cs\_VYmybHX]), 13:00 - 15:00 (Booked by Stefan [C118 | cs\_gZ6a2fB])
* **Date:** Tuesday, 2019-04-09
* **Agenda:** Let's figure out hardware. Pt.3 (Building electronics!)

### Confirmed Attendees
- [x] @Snuggle
- [x] @StefanOid
- [ ] @CastorOfEarth
- [ ] @@OLI243

## Last Meeting

 * **Location:** Millennium Point, Level 4 Project Space
 * **Time:** 11:00 - 14:40
 * **Date:** Thursday, 2019-04-04
 * **Agenda:** Let's figure out hardware. Pt.2

### Confirmed Attendees
 - [x] @Snuggle
 - [x] @StefanOid
 - [ ] @CastorOfEarth
 - [ ] @@OLI243

## ~~*A Previous Project Meeting*~~
* **Location:** Millennium Point, Level 4 Project Space
* **Time:** 11:00 - "Whenever we get bored."
* **Date:** Tuesday, 2019-04-02
* **Agenda:** Let's figure out hardware.
### Confirmed Attendees
- [x] @Snuggle
- [x] @StefanOid
- [ ] @CastorOfEarth
- [ ] @@OLI243